"use strict";
var middleware = require("cookie-parser");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:cookieparser");
  debug("start");

  debug(".useSetup");
  imports.express.useSetup(middleware());

  debug("register nothing");
  register(null, {});
};
